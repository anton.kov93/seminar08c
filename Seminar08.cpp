﻿#include <iostream>

void FindOddNumbers(int N, bool IsOdd)
{
    int start = IsOdd ? 0 : 1;

    for (start; start <= N; start += 2)
    {
        std::cout << start << std::endl;
    }
}

int main()
{
    const int N = 50;
    
    // Вывод чётных чисел от 0 до N
    for (int i = 0; i <= N; i += 2) 
    {
        std::cout << i << std::endl;
    }
    std::cout << std::endl;

    // Вывод нечётных чисел от 0 до N с использованием функции
    FindOddNumbers(N, false);
}



